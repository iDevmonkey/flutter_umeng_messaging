
import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';
import 'package:platform/platform.dart';

typedef Future<dynamic> MessageHandler(Map<String, dynamic> message);

class FlutterUmengMessaging {
  factory FlutterUmengMessaging() => _instance;

  @visibleForTesting
  FlutterUmengMessaging.private(MethodChannel channel, Platform platform)
      : _channel = channel,
        _platform = platform;

  static final FlutterUmengMessaging _instance = FlutterUmengMessaging.private(
      const MethodChannel('plugins.flutter.io/flutter_umeng_messaging'),
      const LocalPlatform());

  final MethodChannel _channel;
  final Platform _platform;

  MessageHandler _onMessage;
  MessageHandler _onBackgroundMessage;
  MessageHandler _onLaunch;
  MessageHandler _onResume;

  /// Setup
  /// On iOS, the params is {"appkey":"", "channel":"AppStore", "logEnabled": true, "encryptEnabled": false}
  /// On android, the params is {"appkey":"", "messageSecret": "", "channel":"umeng", "logEnabled": true, "encryptEnabled": false}
  ///
  Future<bool> setup(Map params) {
    if (params == null) return null;
    return _channel.invokeMethod<bool>('setup', params);
  }

  /// On iOS, prompts the user for notification permissions the first time
  /// it is called.
  ///
  /// Does nothing and returns null on Android.
  FutureOr<bool> requestNotificationPermissions([
    IosNotificationSettings iosSettings = const IosNotificationSettings(),
  ]) {
    if (!_platform.isIOS) {
      return null;
    }
    return _channel.invokeMethod<bool>(
      'requestNotificationPermissions',
      iosSettings.toMap(),
    );
  }

  /// Sets up [MessageHandler] for incoming messages.
  void configure({
    MessageHandler onMessage,
    MessageHandler onBackgroundMessage,
    MessageHandler onLaunch,
    MessageHandler onResume,
  }) {
    _onMessage = onMessage;
    _onLaunch = onLaunch;
    _onResume = onResume;
    _channel.setMethodCallHandler(_handleMethod);

    if (_platform.isAndroid) {

    }
    if (onBackgroundMessage != null) {
      /*
      _onBackgroundMessage = onBackgroundMessage;
      final CallbackHandle backgroundSetupHandle =
      PluginUtilities.getCallbackHandle(_fcmSetupBackgroundChannel);
      final CallbackHandle backgroundMessageHandle =
      PluginUtilities.getCallbackHandle(_onBackgroundMessage);

      if (backgroundMessageHandle == null) {
        throw ArgumentError(
          '''Failed to setup background message handler! `onBackgroundMessage`
          should be a TOP-LEVEL OR STATIC FUNCTION and should NOT be tied to a
          class or an anonymous function.''',
        );
      }

      _channel.invokeMethod<bool>(
        'FcmDartService#start',
        <String, dynamic>{
          'setupHandle': backgroundSetupHandle.toRawHandle(),
          'backgroundHandle': backgroundMessageHandle.toRawHandle()
        },
      );

       */
    }
  }

  final StreamController<String> _tokenStreamController =
  StreamController<String>.broadcast();

  /// when a new device token is generated.
  Stream<String> get onTokenRefresh {
    return _tokenStreamController.stream;
  }

  /// iOS only

  Future<void> setAutoAlert(bool enable) {
    if (!_platform.isIOS) {
      return null;
    }
    return _channel.invokeMethod<void>('setAutoAlert', enable);
  }

  Future<void> setBadgeClear(bool enable) {
    if (!_platform.isIOS) {
      return null;
    }
    return _channel.invokeMethod<void>('setBadgeClear', enable);
  }

  /// _handleMethod

  Future<dynamic> _handleMethod(MethodCall call) async {
    switch (call.method) {
      case "onToken":
        final String token = call.arguments;
        _tokenStreamController.add(token);
        return null;
      case "onMessage":
        return _onMessage(call.arguments.cast<String, dynamic>());
      case "onBackgroundMessage":
        return _onBackgroundMessage(call.arguments.cast<String, dynamic>());
      case "onLaunch":
        return _onLaunch(call.arguments.cast<String, dynamic>());
      case "onResume":
        return _onResume(call.arguments.cast<String, dynamic>());
      default:
        throw UnsupportedError("Unrecognized JSON message");
    }
  }
}

class IosNotificationSettings {
  const IosNotificationSettings({
    this.sound = true,
    this.alert = true,
    this.badge = true,
    this.provisional = false,
  });

  IosNotificationSettings.fromMap(Map<String, bool> settings)
      : sound = settings['sound'],
        alert = settings['alert'],
        badge = settings['badge'],
        provisional = settings['provisional'];

  final bool sound;
  final bool alert;
  final bool badge;
  final bool provisional;

  @visibleForTesting
  Map<String, dynamic> toMap() {
    return <String, bool>{
      'sound': sound,
      'alert': alert,
      'badge': badge,
      'provisional': provisional
    };
  }

  @override
  String toString() => 'PushNotificationSettings ${toMap()}';
}
