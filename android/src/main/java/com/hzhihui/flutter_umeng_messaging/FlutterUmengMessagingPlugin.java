package com.hzhihui.flutter_umeng_messaging;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import com.umeng.commonsdk.UMConfigure;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;

import org.android.agoo.huawei.HuaWeiRegister;
import org.android.agoo.mezu.MeizuRegister;
import org.android.agoo.oppo.OppoRegister;
import org.android.agoo.vivo.VivoRegister;
import org.android.agoo.xiaomi.MiPushRegistar;

import java.util.Map;

/** FlutterUmengMessagingPlugin */
public class FlutterUmengMessagingPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware {
  private static final String channelName = "plugins.flutter.io/flutter_umeng_messaging";
  private static final String TAG = FlutterUmengMessagingPlugin.class.getName();

  protected static Handler mainHandler;

  private MethodChannel channel;
  private Activity activity;
  private PushAgent mPushAgent;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    Log.i(TAG,"onAttachedToEngine");
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), channelName);
    channel.setMethodCallHandler(this);
  }

  public static void registerWith(Registrar registrar) {
    Log.i(TAG,"registerWith");
    final MethodChannel channel = new MethodChannel(registrar.messenger(), channelName);

    FlutterUmengMessagingPlugin plugin = new FlutterUmengMessagingPlugin();
    plugin.channel = channel;
    plugin.activity = registrar.activity();

    channel.setMethodCallHandler(plugin);
  }

  public FlutterUmengMessagingPlugin() {
    if(mainHandler == null){
      mainHandler=new Handler(Looper.getMainLooper());
    }
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull final Result res) {
    final MainThreadResult result=new MainThreadResult(res, mainHandler);

    if (call.method.equals("setup")) {
      setup(call,result);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onAttachedToActivity(ActivityPluginBinding activityPluginBinding) {
    Log.i(TAG,"onAttachedToEngine");
    activity = activityPluginBinding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() { }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding activityPluginBinding) { }

  @Override
  public void onDetachedFromActivity() { }

  /*
   * Handler
   */

  public void setup(MethodCall call, MainThreadResult result) {
    boolean logEnabled = (boolean) call.argument("logEnabled");
    boolean encryptEnabled = (boolean) call.argument("encryptEnabled");
    String appkey = (String) call.argument("appkey");
    String messageSecret = (String) call.argument("messageSecret");
    String channelName = (String) call.argument("channel");

    Log.i(TAG,"setup arguments: " + call.arguments.toString());

    /**
     * Init
     */
    UMConfigure.setLogEnabled(logEnabled);
    UMConfigure.setEncryptEnabled(encryptEnabled);
    UMConfigure.init(activity, appkey, channelName, UMConfigure.DEVICE_TYPE_PHONE, messageSecret);

    /**
     * Register
     */
    mPushAgent = PushAgent.getInstance(activity);
    mPushAgent.register(new IUmengRegisterCallback() {
      @Override
      public void onSuccess(final String deviceToken) {
        Log.i(TAG,"注册成功：deviceToken：-------->  " + deviceToken);
        mainHandler.post(
                new Runnable() {
                  @Override
                  public void run() {
                    channel.invokeMethod("onToken", deviceToken);
                  }
                });
      }

      @Override
      public void onFailure(String s, String s1) {
        Log.e(TAG,"注册失败：-------->  " + "s:" + s + ",s1:" + s1);
      }
    });

    /**
     * Register厂商通道
     */
    Map xiaomi = (Map) call.argument("xiaomi");
    Map meizu = (Map) call.argument("meizu");
    Map oppo = (Map) call.argument("oppo");
    Map vivo = (Map) call.argument("vivo");
    Map huawei = (Map) call.argument("huawei");

    if (xiaomi != null) {
      String appId = (String) xiaomi.get("appId");
      String appKey = (String) xiaomi.get("appKey");

      MiPushRegistar.register(activity, appId, appKey);
    }

    if (meizu != null) {
      String appId = (String) meizu.get("appId");
      String appKey = (String) meizu.get("appKey");

      MeizuRegister.register(activity, appId, appKey);
    }

    if (oppo != null) {
      String appId = (String) oppo.get("appId");
      String appSecret = (String) oppo.get("appSecret");

      OppoRegister.register(activity, appId, appSecret);
    }

    if (vivo != null) {
      VivoRegister.register(activity);
    }

    if (huawei != null) {
      HuaWeiRegister.register(activity.getApplication());
    }

    result.success(true);
  }

  /**
   * MainThreadResult
   */

  private static class MainThreadResult implements MethodChannel.Result {
    private MethodChannel.Result result;
    private Handler handler;

    MainThreadResult(MethodChannel.Result result,Handler mainHandler) {
      this.result = result;
      handler = mainHandler;
    }

    @Override
    public void success(final Object res) {
      handler.post(
              new Runnable() {
                @Override
                public void run() {
                  result.success(res);
                }
              });
    }

    @Override
    public void error(
            final String errorCode, final String errorMessage, final Object errorDetails) {
      handler.post(
              new Runnable() {
                @Override
                public void run() {
                  result.error(errorCode, errorMessage, errorDetails);
                }
              });
    }


    @Override
    public void notImplemented() {
      handler.post(
              new Runnable() {
                @Override
                public void run() {
                  result.notImplemented();
                }
              });
    }
  }
}

