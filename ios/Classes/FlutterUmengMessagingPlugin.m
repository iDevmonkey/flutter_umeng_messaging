#import "FlutterUmengMessagingPlugin.h"

#import <UMCommon/UMCommon.h>
#import <UMPush/UMessage.h>

static NSObject<FlutterPluginRegistrar> *_registrar;

@interface FlutterUmengMessagingPlugin ()<UNUserNotificationCenterDelegate>

@end

@implementation FlutterUmengMessagingPlugin
{
  FlutterMethodChannel *_channel;
  NSDictionary *_launchOptions;
  BOOL _resumingFromBackground;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    _registrar = registrar;
    FlutterMethodChannel* channel = [FlutterMethodChannel
        methodChannelWithName:@"plugins.flutter.io/flutter_umeng_messaging"
              binaryMessenger:[registrar messenger]];
    FlutterUmengMessagingPlugin* instance = [[FlutterUmengMessagingPlugin alloc] initWithChannel:channel];
    
    [registrar addApplicationDelegate:instance];
    [registrar addMethodCallDelegate:instance channel:channel];
}

- (instancetype)initWithChannel:(FlutterMethodChannel *)channel {
    self = [super init];

    if (self) {
      _channel = channel;
      _resumingFromBackground = NO;
    }
    return self;
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    if ([@"setup" isEqualToString:call.method]) {
        [self setup:call result:result];
    }
    else if ([@"requestNotificationPermissions" isEqualToString:call.method]) {
        [self requestNotificationPermissions:call result:result];
    }
    else if ([@"setAutoAlert" isEqualToString:call.method]) {
        [self setAutoAlert:call result:result];
    }
    else if ([@"setBadgeClear" isEqualToString:call.method]) {
        [self setBadgeClear:call result:result];
    }
    else {
      result(FlutterMethodNotImplemented);
    }
}

#pragma mark - Handler

- (void)setup:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSDictionary *arguments = call.arguments;
    
    NSLog(@"**********UMengPush setup, arguments: %@", arguments);

    [UMConfigure setLogEnabled:[arguments[@"logEnabled"] boolValue]];
    [UMConfigure setEncryptEnabled:[arguments[@"encryptEnabled"] boolValue]];
    [UMConfigure initWithAppkey:arguments[@"appkey"] channel:arguments[@"channel"]];
    
    result(@YES);
}

- (void)requestNotificationPermissions:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSDictionary *arguments = call.arguments;
    
    NSLog(@"**********UMengPush requestNotificationPermissions, arguments: %@", arguments);

    UMessageAuthorizationOptions authOptions = UMessageAuthorizationOptionNone;
    if ([arguments[@"sound"] boolValue]) {
      authOptions |= UMessageAuthorizationOptionSound;
    }
    if ([arguments[@"alert"] boolValue]) {
      authOptions |= UMessageAuthorizationOptionAlert;
    }
    if ([arguments[@"badge"] boolValue]) {
      authOptions |= UMessageAuthorizationOptionBadge;
    }
    
    UMessageRegisterEntity *entity = [[UMessageRegisterEntity alloc] init];
    entity.types = authOptions;
    
    [UNUserNotificationCenter currentNotificationCenter].delegate=self;
    [UMessage registerForRemoteNotificationsWithLaunchOptions:_launchOptions Entity:entity completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if (granted) {
            NSLog(@"**********UMengPush register success.");
            result(@YES);
        }else{
            NSLog(@"**********UMengPush register failed, error: %@", error.localizedDescription);
            result(@YES);
        }
    }];
}

- (void)setAutoAlert:(FlutterMethodCall*)call result:(FlutterResult)result {
    [UMessage setAutoAlert:[call.arguments boolValue]];
}

- (void)setBadgeClear:(FlutterMethodCall*)call result:(FlutterResult)result {
    [UMessage setBadgeClear:[call.arguments boolValue]];
}

- (void)didReceiveRemoteNotification:(NSDictionary *)userInfo {
  if (_resumingFromBackground) {
      NSLog(@"onResume:%@",userInfo);
      [_channel invokeMethod:@"onResume" arguments:userInfo];
  } else {
      NSLog(@"onMessage:%@",userInfo);
      [_channel invokeMethod:@"onMessage" arguments:userInfo];
  }
}

#pragma mark - AppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    _launchOptions = launchOptions;
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
  _resumingFromBackground = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
  _resumingFromBackground = NO;
  application.applicationIconBadgeNumber = 0;
}

- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [self _stringForDeviceToken:deviceToken];
    
    NSLog(@"**********deviceToken:%@",token);
    
    if (token != nil) {
        [_channel invokeMethod:@"onToken" arguments:token];
    }
}

- (BOOL)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo
          fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler {
    
    [UMessage setAutoAlert:NO];
    
    [self didReceiveRemoteNotification:userInfo];
    
    if(![userInfo valueForKeyPath:@"aps.recall"])
    {
        completionHandler(UIBackgroundFetchResultNewData);
    }
    return YES;
}

#pragma mark - UNUserNotificationCenterDelegate

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        // 应用处于前台时的远程推送接收
        NSLog(@"「应用处于前台」远程推送接收：%@", userInfo);
        
        [UMessage setAutoAlert:NO];
        [UMessage didReceiveRemoteNotification:userInfo];
    }
    else{
        // 应用处于前台时的本地推送接受
        NSLog(@"「应用处于前台」本地推送接收：%@", userInfo);
    }
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if(state == UIApplicationStateActive){
        [_channel invokeMethod:@"onMessage" arguments:userInfo];
    }
    else if (state == UIApplicationStateInactive){
        [_channel invokeMethod:@"onResume" arguments:userInfo];
    }
    
    completionHandler(UNNotificationPresentationOptionSound|UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionAlert);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
    didReceiveNotificationResponse:(UNNotificationResponse *)response
             withCompletionHandler:(void (^)(void))completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    
    if([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        //应用处于后台时的远程推送接受
        NSLog(@"「应用处于后台」远程推送接收：%@", userInfo);
        
        [UMessage didReceiveRemoteNotification:userInfo];
    }
    else{
        //应用处于后台时的本地推送接受
        NSLog(@"「应用处于后台」本地推送接收：%@", userInfo);
    }
    
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if(state == UIApplicationStateBackground){
        [_channel invokeMethod:@"onBackgroundMessage" arguments:userInfo];
    }
    else if (state == UIApplicationStateInactive){
        [_channel invokeMethod:@"onResume" arguments:userInfo];
    }
    
    completionHandler();
}

#pragma mark - Private Function

- (NSString *)_stringForDeviceToken:(NSData *)deviceToken {
    if (![deviceToken isKindOfClass:[NSData class]]) return nil;
   
    const unsigned *tokenBytes = (const unsigned *)[deviceToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                          ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                          ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                          ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];
    return hexToken;
}

@end
